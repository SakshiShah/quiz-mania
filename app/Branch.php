<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Branch extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'name',
    ];

    /**
     * RELATIONSHIP METHODS
     */
    public function semesters(){
        return $this->belongsToMany(Semester::class, 'branch_semester_subject', 'semester_id', 'branch_id');
    }

    /**
     * SCOPE METHODS
     */

    public function scopeSearch($query, $searchValue){
        if($searchValue){
            return $query->where('name', 'like', "%{$searchValue}%");
        }
        return $query;
    }

    public function scopeOrder($query, $orderBy){
        if($orderBy){
            $columns = ["sr_no", "name", "actions"];
            return $query->orderBy($columns[$orderBy[0]['column']], $orderBy[0]['dir']);
        }
        return $query;
    }

    public function scopeLimitBy($query, $start, $length){
        if($length != -1){
            return $query->offset($start)->limit($length);
        }
        return $query;
    }


    /**
     * METHODS FOR DATATABLE 
     */
    public static function getData($draw, $searchParameter, $orderBy, $start, $length){
        $filteredData = Branch::withoutTrashed()->search($searchParameter)->order($orderBy)->limitBy($start, $length)->get();
        $numberOfTotalRows = Branch::count();
        $numberOfFilteredRows = Branch::withoutTrashed()->search($searchParameter)->get()->count();
        $data = array();
        for($i = 0; $i < sizeof($filteredData); $i++)
        {
            $subarray = [];
            $subarray[] = $i + 1;
            $subarray[] = $filteredData[$i]->name;
            $subarray[] = <<<BUTTONS
            <button class='edit btn btn-warning btn-just-icon' data-id='{$filteredData[$i]->id}' data-toggle='modal' data-target='#editModal' onclick="displayEditModalForm('{$filteredData[$i]->id}')"><i class="nc-icon nc-ruler-pencil mr-1"></i> Edit</button>
            <button class='delete btn btn-danger btn-just-icon' data-id='{$filteredData[$i]->id}' data-toggle='modal' data-target='#deleteModal' onclick="displayDeleteModalForm('{$filteredData[$i]->id}')"><i class='nc-icon nc-box mr-1'></i> Delete</button>
BUTTONS;
            $data[] = $subarray;
        }

        $output = array(
            "draw"=>$draw,
            "recordsTotal"=>$numberOfTotalRows,
            "recordsFiltered"=>$numberOfFilteredRows,
            "data"=> $data
        );

        echo json_encode($output);
    }


}
