<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chapter extends Model
{
    protected $fillable = [
        'name', 
        'weightage'
    ];

    /**
     * RELATIONSHIP METHODS
     */
    public function subjects()
    {
        return $this->belongsToMany(Subject::class)->withTimestamps();
    }

    public function questions()
    {
        return $this->belongsToMany(Question::class);
    }

    public function test()
    {
        return $this->belongsToMany(Test::class)->withPivot('weightage_percentage');;
    }
}
