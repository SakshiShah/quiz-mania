<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CorrectOption extends Model
{
    protected $fillable = [
        'question_id',
        'option_id'
    ];
}
