<?php
namespace App\Classes;

use App\Option;
use App\Question;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PDO;

class QuestionsHelper{
    private $request;
    private $chapterId = array();
    private $chapterMarks = array();
    private $totalMarks;
    private $twoMarksQuestionCount;
    private $fourMarksQuestionCount;
    private $weightages = array();
    private $chapterMarkDetails = array();
    private $questions = array();
    private $finalQuestionPaper;
    // private $total_marks = 0;

    public function generateQuestions($chapter_id, $chapter_marks, $total_marks, $two_marks_question_count, $four_marks_question_count){
        $this->chapterId = $chapter_id;
        $this->chapterMarks = $chapter_marks;
        $this->totalMarks = $total_marks; 
        $this->twoMarksQuestionCount = $two_marks_question_count;
        $this->fourMarksQuestionCount = $four_marks_question_count; 

        $this->fillWeightages();
        $this->fillChapterDetails();
        // dd($this->getQuestions());
        return $this->getQuestions();
    }

    public function fillWeightages(){
        for($i = 0; $i < sizeof($this->chapterId); $i++){
            $marks = ((intval($this->chapterMarks[$i]))/100)*(intval($this->totalMarks));
            $this->weightages[$i] = $marks;
        }
    }

    public function fillChapterDetails(){
        for($i = 0; $i < sizeof($this->chapterId); $i++){
            $this->chapterMarkDetails[$this->chapterId[$i]]["min_marks"] = $this->weightages[$i];
            $this->chapterMarkDetails[$this->chapterId[$i]]["marks_completed"] = 0;
        }        
    }

    private function getQuestions(){
        // $this->finalQuestionPaper["name"] = $this->request->name;
        // $this->finalQuestionPaper["total_marks"] = $this->request->total_marks;
        // $this->finalQuestionPaper["branch"] = $this->request->branch;
        // $this->finalQuestionPaper["semester"] = $this->request->semester;
        // $this->finalQuestionPaper["subject"] = $this->request->subjects;
        // $this->finalQuestionPaper["two_marks_question"] = $this->request->two_marks_question_count;
        // $this->finalQuestionPaper["four_marks_question"] = $this->request->four_marks_question_count;
        // $this->finalQuestionPaper['duration'] = $this->request->duration;
        // $this->finalQuestionPaper['scheduled_at'] = $this->request->scheduled_at;
        // $this->finalQuestionPaper['chapter_id'] = $this->request->chapter_id;

        $questionsOf2Marks = $this->generateQuestion(2, $this->twoMarksQuestionCount);
        $questionsOf4Marks = $this->generateQuestion(4, $this->fourMarksQuestionCount);
        // if($questionsOf2Marks == null && $questionsOf4Marks == null){
        //     return null;
        // }
        // $questions = array_merge($questionsOf2Marks, $questionsOf4Marks);
        $questions["2-marks"] = $questionsOf2Marks;
        $questions["4-marks"] = $questionsOf4Marks;
        $data["questions"] = $questions;
        // dd($questions);
        $this->finalQuestionPaper[] = $data;
        // dd($data);
        return $this->finalQuestionPaper;
        
    }

    private function getQuestionByChapterID($chapter_id, $marksPerSubQuestion, $difficulty){

        // $questionCondition = "";
        // for($i = 0; is_array($this->questions) && $i < sizeof($this->questions); $i++){
        //     $questionCondition .= " AND questions.id != {$this->questions[$i]}";
        // }
        // $sql = "SELECT * FROM questions INNER JOIN chapter_question ON chapter_question.chapter_id = {$chapter_id} AND chapter_question.question_id = questions.id  WHERE questions.marks = {$marksPerSubQuestion} AND questions.status = '{$difficulty}' {$questionCondition} ORDER BY RAND()";

        // // echo "Query : ".$sql;
        // $question = DB::select(DB::raw($sql));
        $questions = Question::join("chapter_question", function($join) use($chapter_id){
                        $join->on("chapter_question.question_id", '=', "questions.id")->where("chapter_question.chapter_id", '=', $chapter_id);
                    })->where("questions.marks", '=', $marksPerSubQuestion)
                    ->where("questions.status", '=', $difficulty)
                    ->ignoreQuestions($this->questions)
                    ->inRandomOrder()
                    ->get();
        // dd($questions);
        // print_r($question);
        return $questions;
    }

    private function generateQuestion($marks, $questionCount){
        
        $questionTitle = array();
        
        $questionTitle = $this->questionMarkCount($marks, $questionCount);
    
        // dd($questionTitle['easy']['question']);
        if( ($questionTitle && is_array($questionTitle["easy"]) && is_array($questionTitle["medium"]) && is_array($questionTitle["hard"])) && ((sizeof($questionTitle["easy"]["question"]) + sizeof($questionTitle["medium"]["question"]) + sizeof($questionTitle["hard"]["question"])) >= $questionCount)){
            return $questionTitle;
        }
        return NULL;
        
    }
    
    private function getQuestionChapterID($ignoreChapters = array()){
        $flag = false;
        for($i = 0; $i<sizeof($this->chapterMarkDetails); $i++){
            if($flag == false && in_array(intval($this->chapterId[$i]), $ignoreChapters) == false){
                $chapterWithMostMarksLeft = intval($this->chapterId[$i]);
                $mostMarks = $this->chapterMarkDetails[$this->chapterId[$i]]["min_marks"] - $this->chapterMarkDetails[$this->chapterId[$i]]["marks_completed"];
                $flag = true;
            }
            if(in_array(intval($this->chapterId[$i]), $ignoreChapters)){
                continue;
            }
            if($mostMarks < $this->chapterMarkDetails[$this->chapterId[$i]]["min_marks"] - $this->chapterMarkDetails[$this->chapterId[$i]]["marks_completed"]){
                $mostMarks = $this->chapterMarkDetails[$this->chapterId[$i]]["min_marks"] - $this->chapterMarkDetails[$this->chapterId[$i]]["marks_completed"];
                $chapterWithMostMarksLeft = intval($this->chapterId[$i]);
            }
        }
        return $chapterWithMostMarksLeft;
    }

    private function questionMarkCount($marks, $questionCount){ 
        $numberOfChapters = sizeof($this->chapterId);
        $status = ["easy", "medium", "hard"];        
        $questionTitle = [];

        $failCounter = 0;
        // dd($numberOfChapters);
        $ignoreChapters = array();
        $i = 0;        
        while($i < $questionCount){
            $i++;
            $chapter_id = $this->getQuestionChapterID($ignoreChapters);     
            foreach($status as $state){
                $questions = $this->getQuestionByChapterID($chapter_id, $marks, $state);
                if(sizeof($questions) == 0){
                    $ignoreChapters[] = $chapter_id;
                    $failCounter++;
                    if($failCounter == $numberOfChapters){
                        return null;
                    }
                    continue;
                }
                $this->totalMarks += $marks;
                $this->questions[] = intval($questions[0]->id);
                $id = intval($questions[0]->id);
                $questionTitle[$state]["question"][strval($questions[0]->id)] = $questions[0]->statement;
                $questionTitle[$state]["options"][strval($questions[0]->id)] = $this->getOptions($questions[0]->id);
                $questionTitle[$state]["correct-option"][strval($questions[0]->id)] = $this->getCorrectOption($questions[0]->id);
                $questionTitle["question-count"] = $questionCount;
                $this->chapterMarkDetails[strval($chapter_id)]["marks_completed"] += $marks;
            }       
        }
        return $questionTitle;
    }

    private function getOptions($question_id){

        $optionArray = Option::where("question_id", "=", $question_id)->get();
        $options = [];
        foreach($optionArray as $option){
            $options[strval($option->id)] = $option->statement;
        }
        return $options;
    }

    private function getCorrectOption($question_id){
        $correctOption = DB::table("correct_options")->where('question_id', '=', $question_id)->pluck('option_id')->first();
        return $correctOption;
    }
}                   