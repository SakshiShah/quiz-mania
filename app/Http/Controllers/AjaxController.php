<?php

namespace App\Http\Controllers;

use App\Branch;
use App\Classes\QuestionsHelper;
use App\Subject;
use App\Test;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\VarDumper\VarDumper;

class AjaxController extends Controller
{
    public function getData(Request $request){

        if($request->has('page')){

            $search_parameter = $request->get('search')['value'];
            $order_by = $request->get('order');
            $start = $request->get('start');
            $length = $request->get('length');
            $draw = $request->get('draw');

            if($request->get('page') == 'manage_tests') {
                return Test::getData($draw, $search_parameter, $order_by, $start, $length);
            }
            if($request->get('page') == 'manage_branches') {
                return Branch::getData($draw, $search_parameter, $order_by, $start, $length);
            }

            if($request->get('page') == 'create_test') {
                if($request->has('branchId')){

                    $branchId = $request->get('branchId');
                    $semesterId = $request->get('semesterId');

                    return $this->getSubjects($branchId, $semesterId);

                }elseif($request->has('subjectId')){

                    $subject = Subject::find($request->get('subjectId'));
                    return json_encode($subject->chapters);
                }
            }

            if($request->get('page') == 'edit_test'){
                if($request->has('testId')){

                    $testId = $request->get('testId');

                    $test = Test::find($testId);
                    $test_chapters = $test->chapters;
                    $data = [];
                    foreach($test_chapters as $test_chapter)
                    {
                        $data[] = [
                            "chapter_name" => $test_chapter->name,
                            "weightage_percentage" => $test_chapter->pivot->weightage_percentage
                        ];
                    }
                    return json_encode($data);
                }
            }

            if($request->get('page') == 'start_test')
            {
                if($request->has('testId'))
                {
                    $testId = $request->get('testId');

                    $test = Test::find($testId);
                    $chapter_id = []; 
                    $chapter_marks= [];
                    foreach($test->chapters as $chapter)
                    {
                        $chapter_id[] = $chapter->id;
                        $chapter_marks[] = $chapter->pivot->weightage_percentage;
                    }
                    // dd($chapter_id, $chapter_marks);

                    $total_marks = $test->total_marks;
                    $two_marks_question_count = $test->two_marks_question_count;
                    $four_marks_question_count = $test->four_marks_question_count;

                    $questionObj = new QuestionsHelper();
                    $questions = $questionObj->generateQuestions($chapter_id, $chapter_marks, 
                                                            $total_marks, $two_marks_question_count, $four_marks_question_count);

                    return json_encode($questions[0]['questions']);
                }
            }
        }

        if($request->has('fetch'))
        {
            if($request->get('fetch') == 'branch')
            {
                $branchId = $request->get('branch_id');
                $branch_name = Branch::findOrFail($branchId)->name;
                return json_encode($branch_name);
            }
        }
    }

    private function getSubjects($branchId, $semesterId)
    {
        $branch = Branch::find($branchId);
        $subjectIds = DB::table('branch_semester_subject')->where('branch_id', $branchId)->where('semester_id', $semesterId)->pluck('subject_id')->toArray();
        $subjects = Subject::whereIn('id', $subjectIds)->get(); 

        return json_encode($subjects);
    }

    public function getResult(Request $request)
    {
        if($request->has('page') == 'test_result')
        {
            if($request->has('testId'))
            {
                $testId = $request->get('testId');
                $data = $request->get('testData');
                dd($data[0]);
                return json_encode($data);
            }
        }
    }
}
