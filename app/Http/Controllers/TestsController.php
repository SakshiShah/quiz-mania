<?php

namespace App\Http\Controllers;

use App\Branch;
use App\Http\Requests\CreateTestRequest;
use App\Semester;
use App\Student;
use App\Test;
use Illuminate\Http\Request;
use App\Classes\QuestionsHelper;
use App\Http\Requests\UpdateTestRequest;
use App\Subject;
use App\User;
use Carbon\Traits\Test as TraitsTest;
use Illuminate\Support\Facades\DB;

class TestsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tests = [];
        if(!auth()->user()->isAdmin()){
            $student = Student::where('user_id', auth()->user()->id)->first();
            $tests = $student->getTests();
        }
        return view('test.index', compact(['tests']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(auth()->user()->isAdmin())
        {
            $branches = Branch::all();
            $semesters = Semester::all();
            return view('test.create', compact([
                'branches',
                'semesters'
            ]));
        }
        abort(403);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateTestRequest $request)
    {
        // dd($request);

        $branch_semester_subject_id = DB::table('branch_semester_subject')
                                    ->select('id')
                                    ->where('branch_id', $request->branch)
                                    ->where('semester_id', $request->semester)
                                    ->where('subject_id', $request->subjects)
                                    ->first()->id;

        $questionObj = new QuestionsHelper();
        // dd($request);
        $questions = $questionObj->generateQuestions($request->chapter_id, $request->chapter_marks, 
                                                    $request->total_marks, $request->two_marks_question_count, $request->four_marks_question_count);
        // dd($questions);

        if($questions){
            $test = Test::create([
                "name" => $request->name,
                "branch_semester_subject_id" => $branch_semester_subject_id,
                "two_marks_question_count" => $request->two_marks_question_count,
                "four_marks_question_count" => $request->four_marks_question_count,
                "total_marks" => $request->total_marks,
                "duration" => $request->duration,
                "created_by" => auth()->user()->id,
                "scheduled_at" => $request->scheduled_at,
            ]);
    
            $i = 0;
            foreach($request->chapter_id as $chapter_id){
                DB::table('chapter_test')->insert([
                    "test_id" => $test->id,
                    "chapter_id" => $chapter_id,
                    'weightage_percentage' =>  $request->chapter_marks[$i]    
                ]);
                $i++;
            }
        }

        return redirect(route('tests.index'));

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Test  $test
     * @return \Illuminate\Http\Response
     */
    public function show(Test $test)
    {
        $branch_semester_subject_id = $test->branch_semester_subject_id;
        $data = DB::table('branch_semester_subject')
                    ->select('branch_id', 'semester_id', 'subject_id')
                    ->where('id', $branch_semester_subject_id)
                    ->first();

        $branch = Branch::find($data->branch_id);
        $semester = Semester::find($data->semester_id);
        $subject = Subject::find($data->subject_id);

        $weightages = DB::table('chapter_test')
                            ->select('weightage_percentage')
                            ->where('test_id', $test->id)
                            ->get();

        $chapters = $test->chapters;
        $chapter_weightages = [];
        for ($i=0; $i <$chapters->count() ; $i++) { 
            $chapter_weightages[] = [
                'chapter_name' => $chapters[$i]->name,
                'wieghtage' => $weightages[$i]->weightage_percentage
            ];
        }

        return view('test.show', compact([
            'test',
            'branch',
            'semester',
            'subject',
            'chapter_weightages'
        ]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Test  $test
     * @return \Illuminate\Http\Response
     */
    public function edit(Test $test)
    {
        $branches = Branch::all();
        $semesters = Semester::all();

        $branch_semester_subject_id = $test->branch_semester_subject_id;
        $data = DB::table('branch_semester_subject')
                    ->select('branch_id', 'semester_id', 'subject_id')
                    ->where('id', $branch_semester_subject_id)
                    ->first();

        $branch_id = Branch::find($data->branch_id)->id;

        $semester_id = Semester::find($data->semester_id)->id;
        $subject_id = Subject::find($data->subject_id)->id;

        return view('test.edit', compact([
            'test',
            'branches',
            'semesters',
            'branch_id',
            'semester_id',
            'subject_id',
        ]));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Test  $test
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTestRequest $request, Test $test)
    {
        // dd($request);
        $branch_semester_subject_id = DB::table('branch_semester_subject')
                                    ->select('id')
                                    ->where('branch_id', $request->branch)
                                    ->where('semester_id', $request->semester)
                                    ->where('subject_id', $request->subjects)
                                    ->first()->id;

        $questionObj = new QuestionsHelper();
        $questions = $questionObj->generateQuestions($request->chapter_id, $request->chapter_marks, 
                                                    $request->total_marks, $request->two_marks_question_count, $request->four_marks_question_count);
        if($questions)
        {

            $test->update([
                "name" => $request->name,
                "branch_semester_subject_id" => $branch_semester_subject_id,
                "two_marks_question_count" => $request->two_marks_question_count,
                "four_marks_question_count" => $request->four_marks_question_count,
                "total_marks" => $request->total_marks,
                "duration" => $request->duration,
                "created_by" => auth()->user()->id,
                "scheduled_at" => $request->scheduled_at,
            ]);

            DB::table('chapter_test')
                ->where('test_id', $test->id)
                ->delete();

            // dd("here");
            $i = 0;
            foreach($request->chapter_id as $chapter_id){
                DB::table('chapter_test')->insert([
                    "test_id" => $test->id,
                    "chapter_id" => $chapter_id,
                    'weightage_percentage' =>  $request->chapter_marks[$i]    
                ]);
                $i++;
            }
            // dd("here");
        }

        return redirect(route('tests.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Test  $test
     * @return \Illuminate\Http\Response
     */
    public function destroy(Test $test)
    {
        //
    }

    public function startTest(Request $request)
    {
        $test = Test::findOrFail($request->get('test-id'));

        $subject_id = DB::table('branch_semester_subject')
                        ->where('id', $test->branch_semester_subject_id)
                        ->first()->subject_id;

        $subject_name = Subject::find($subject_id)->name;
        return view('test.start-test', compact([
            'test',
            'subject_name'
        ]));
    }
}
