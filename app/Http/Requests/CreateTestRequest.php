<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateTestRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->isAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'subjects' => 'required',
            'chapter_id' => 'required',
            'total_marks' => 'required',
            'two_marks_question_count' => 'required',
            'four_marks_question_count' => 'required',
            'duration' => 'required'
        ];
    }
}
