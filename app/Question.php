<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Question extends Model
{
    
    protected $fillable = [
        'statement',
        'marks',
        'status'
    ];

    /**
     * RELATIONSHIP METHODS
     */
    public function chapter()
    {
        return $this->belongsToMany(Chapter::class);
    }

    public function options()
    {
        return $this->hasMany(Option::class);
    }

    /**
     * SCOPE QUERIES
     */

     public function scopeIgnoreQuestions($query, $questions){
        for($i = 0; is_array($questions) && $i < sizeof($questions); $i++){
            $query = $query->where("questions.id", "!=", $questions[$i]);
        }
    
     }
}
