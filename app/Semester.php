<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Semester extends Model
{
    protected $fillable = [
        'name',
    ];


    public function branch(){
        return $this->belongsToMany(Branch::class, 'branch_semester_subject', 'semester_id', 'branch_id');
    }

    public function subjects(){
        return $this->belongsToMany(Subject::class, 'branch_semester_subject', 'subject_id', 'semester_id');
    }
}
