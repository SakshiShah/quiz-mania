<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Student extends Model
{
    protected $fillable = [
        'user_id',
    ];


    public function tests(){
        return $this->belongsToMany(Test::class);                                                  
    }

    public function getTests(){
        $studentTest = [];

        $enrollment = Enrollment::where('student_id', $this->id)->first();
        // dd($enrollment);
        $student_branch_id = $enrollment->branch_id;
        $student_semester_id = $enrollment->semester_id;
        // dd($student_branch_id);
        $tests = Test::withoutTrashed()->get();
        // dd($tests);
        foreach($tests as $test){
            $branch_semester_subject_id = $test->branch_semester_subject_id;
            // dd($branch_semester_subject_id);
            
            $data = DB::table('branch_semester_subject')
                    ->select('branch_id', 'semester_id')
                    ->where('id', $branch_semester_subject_id)
                    ->first();
                
            // dd($data);
            $test_branch_id = $data->branch_id;
            $test_semester_id = $data->semester_id;

            if($student_branch_id == $test_branch_id && $student_semester_id == $test_semester_id)
            {   
                $studentTest[] = $test;
            }
        }

        return $studentTest;
    }
}
