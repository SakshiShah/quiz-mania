<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Mockery\Matcher\Subset;

class Test extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'name', 
        'branch_semester_subject_id',
        'two_marks_question_count',
        'four_marks_question_count',
        'total_marks', 
        'duration', 
        'scheduled_at', 
        'created_by'
    ];

    /**
     * RELATIONSHIP METHODS
     */
    public function students()
    {
        return $this->belongsToMany(Student::class)->withTimestamps();
        // , 'student_test', 'test_id', 'user_id');
    }

    public function admin()
    {
        return $this->belongsTo(User::class)->withTimeStamps();
    }

    public function chapters()
    {
            return $this->belongsToMany(Chapter::class)->withPivot('weightage_percentage');
    }

    /**
     * SCOPE METHODS
     */

    public function scopeSearch($query, $searchValue){
        if($searchValue){
            return $query->where('name', 'like', "%{$searchValue}%");
        }
        return $query;
    }

    public function scopeOrder($query, $orderBy){
        if($orderBy){
            $columns = ["sr_no", "name", "actions"];
            return $query->orderBy($columns[$orderBy[0]['column']], $orderBy[0]['dir']);
        }
        return $query;
    }

    public function scopeLimitBy($query, $start, $length){
        if($length != -1){
            return $query->offset($start)->limit($length);
        }
        return $query;
    }


    /**
     * METHODS FOR DATATABLE 
     */
    public static function getData($draw, $searchParameter, $orderBy, $start, $length){

        $filteredData = Test::withoutTrashed()->search($searchParameter)->order($orderBy)->limitBy($start, $length)->get();
        $numberOfTotalRows = Test::count();
        $numberOfFilteredRows = Test::withoutTrashed()->search($searchParameter)->get()->count();
        $data = array();
        for($i = 0; $i < sizeof($filteredData); $i++)
        {
            $subarray = [];
            $subarray[] = $i + 1;
            $subarray[] = $filteredData[$i]->name;
            $subarray[] = <<<BUTTONS
            <a href="/tests/{$filteredData[$i]->id}" class='show btn btn-primary btn-just-icon' data-id='{$filteredData[$i]->id}'><i class="nc-icon nc-alert-circle-i mr-1"></i> View</a>
            <a href="/tests/{$filteredData[$i]->id}/edit" class='edit btn btn-warning btn-just-icon' data-id='{$filteredData[$i]->id}'><i class="nc-icon nc-ruler-pencil mr-1"></i> Edit</a>
            <button class='delete btn btn-danger btn-just-icon' data-id='{$filteredData[$i]->id}' data-toggle='modal' data-target='#deleteModal' onclick="displayModalForm('{$filteredData[$i]->id}')"><i class='nc-icon nc-box mr-1'></i> Delete</button>
BUTTONS;
            $data[] = $subarray;
        }

        $output = array(
            "draw"=>$draw,
            "recordsTotal"=>$numberOfTotalRows,
            "recordsFiltered"=>$numberOfFilteredRows,
            "data"=> $data
        );

        echo json_encode($output);
    }
}
