<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Question;
use Faker\Generator as Faker;

$factory->define(Question::class, function (Faker $faker) {
    $marks = [2, 2, 4];
    $status = ['easy', 'middle', 'hard'];
    return [
        'statement' => $faker->sentence,
        'marks' => $marks[rand(0, 2)],
        'status' => $status[rand(0, 2)]
    ];
});
