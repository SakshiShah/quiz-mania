<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Test;
use Faker\Generator as Faker;

$factory->define(Test::class, function (Faker $faker) {
    return [
        'name' => $faker->sentence(rand(2, 4)),
        'branch_semester_subject_id' => 24,
        'two_marks_question_count' => 8,
        'four_marks_question_count' => 1,
        'total_marks' => 20,
        'duration' => rand(1, 3),
        'scheduled_at' => null,
        'created_by' => 1

    ];
});
