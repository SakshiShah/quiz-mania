<?php

use App\Branch;
use App\Semester;
use App\Subject;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BranchSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $branches = ['Computer Technology', 'Computer Engineering', 'Mechanical Engineering'];
        $semesters = [1, 2, 3, 4, 5, 6];
        $subjects = ['English',
        'Basic Science',
        'Basic Mathematics',
        'Fundamentals of ICT',
        'Engineering Graphics',
        'Workshop Practice',
        'Applied Mathmatics',
        'Basic Electronics',
        'Elements of Electrical Engineering',
        'Programming in C',
        'Computer Peripheral and Hardware Maintenance',
        'Web Page Designing with HTML',
        'Object Oriented Programming using C++',
        'Data Structure using C',
        'Computer Graphics',
        'Database Management System',
        'Digital Techniques',
        'Java Programming',
        'Software Engineering',
        'GUI Application Development using VB.net',
        'Data Communication and Computer Network',
        'Microprocessors',
        'Operating Systems',
        'Advanced Java Programming',
        'Software Testing',
        'Envirnmental Studies',
        'Client Side Scripting',
        'Advanced Computer Network',
        'Advanced Database Management System',
        'Programming with Python',
        'Mobile Application Development',
        'Emerging Trends in Computer and Information Technology',
        'Management',
        'Web Based Application Development Using PHP',
        'Network and Information Security',
        'Data Warehousing and Mining Techniques',
        'Enterpreneurship Development'];

        foreach($branches as $branchName)
        {
            $branch = new Branch();
            $branch->name = $branchName;
            $branch->save();
        }

        foreach($semesters as $semesterName)
        {
            $sem = new Semester();
            $sem->name = $semesterName;
            $sem->save();
        }

        foreach($subjects as $subjectName)
        {
            $subject = new Subject();
            $subject->name = $subjectName;
            $subject->save();
        }

        $k = 1;
        for($branch_id = 1; $branch_id <=3; $branch_id++)
        {
            for($sem_id = 1; $sem_id <= 6; $sem_id++)
            {
                for($subject_id=$k; $subject_id<=36; $subject_id++){
            
                    DB::table('branch_semester_subject')->insert([
                        'branch_id'=> $branch_id,
                        'semester_id' => $sem_id,
                        'subject_id' => $subject_id,
                        'created_at' => now(),
                        'updated_at' => now()
                    ]);
                    if($subject_id%6 == 0){
                        $k += 6;
                        break;
                    }
                }
            }
        }
    }
}
