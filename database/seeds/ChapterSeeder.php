<?php

use App\Subject;
use App\Chapter;
use App\CorrectOption;
use App\Option;
use App\Question;

use Illuminate\Database\Seeder;

class ChapterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $chapters = ["Introduction on and Syntax of Python Program",
                    "Python Operators and Control Flow statements",
                    "Datastructures in Python",
                    "Python Functions, modules and Packages",
                    "Object Oriented Programming in Python",      
                    "File I/O Handling and Exception Handling",
        ];
        $subject = Subject::find(30); //Python
        foreach($chapters as $chapterName)
        {
            $chapter = new Chapter();
            $chapter->name = $chapterName;
            $chapter->weightage = rand(8, 20);
            $chapter->save();
            $subject->chapters()->attach([$chapter->id]);
        }


        $chapters = ["Android and its tools",
                    "Installation and configuration of Android",
                    "UI Components and Layouts",
                    "Designing User Interface with View",
                    "Activity and Multimedia with database",
                    "Security and Application Deployment"
        ];
        $subject = Subject::find(31); //Android
        foreach($chapters as $chapterName)
        {
            $chapter = new Chapter();
            $chapter->name = $chapterName;
            $chapter->weightage = rand(9, 20);
            $chapter->save();
            $subject->chapters()->attach([$chapter->id]);
        }


        $chapters = [
                        "Expressions and control statements in PHP",
                        "Arrays, Functions and Graphics",
                        "Apply Object Oriented Concepts in PHP",
                        "Creating and validating forms",
                        "Database Operations",
                    ];
        $subject = Subject::find(34); //PHP
        foreach($chapters as $chapterName)
        {
            $chapter = new Chapter();
            $chapter->name = $chapterName;
            $chapter->weightage = rand(8, 20);
            $chapter->save();
            $subject->chapters()->attach([$chapter->id]);
        }

        $subject = Subject::find(24); //Advance Java
        $chapters = ["Abstact Window Toolkit(AWT)", 
                    "Swings", 
                    "Event Handling", 
                    "Networks Basics", 
                    "Interacting with Database", 
                    "Servlets"];
        foreach($chapters as $chapterName)
        {
            $chapter = new Chapter();
            $chapter->name = $chapterName;
            $chapter->weightage = rand(8, 20);
            $chapter->save();
            $subject->chapters()->attach([$chapter->id]);
        }
    }
}
