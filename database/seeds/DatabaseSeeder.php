<?php

use App\CorrectOption;
use App\Enrollment;
use App\Test;
use App\User;
use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class, 1)->make()
            ->each(function($user){
                $user->role = 'admin';
                $user->save();
            });

        factory(Test::class, 2)->create();
        

        //For branch, sems and subjects
        $this->call(BranchSeeder::class);

        //For chapters, questions and options
        $this->call(ChapterSeeder::class);

        //For questions
        $this->call(QuestionSeeder::class);

        //For options
        $this->call(OptionSeeder::class);
        
        //For correctOption
        $this->call(CorrectOptionSeeder::class);

        factory(User::class, 5)->create()
            ->each(function($user){
                $student = new App\Student();
                $student->user_id = $user->id;
                $student->save();
                $student->tests()->attach([rand(1, 2)]);          

                Enrollment::create([
                    'student_id' => $student->id,
                    'branch_id' => rand(1, 3),
                    'semester_id' => rand(1, 6)
                ]);
            });
    }
}
