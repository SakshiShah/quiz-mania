var TableDataTables = function(){
    var handleBranchTable = function(){
        var manageBranchTable = $("#branch-datatable");
        var baseURL = window.location.origin;
        var filePath = "/ajax";
        var oTable = manageBranchTable.dataTable({
            "processing" : true,
            "serverSide" : true,
            "paging": true,
            "scrollX": false,
            "ajax" : {
                url : baseURL + filePath,
                method : "POST",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data : {
                    "page": "manage_branches"
                }
            },
            "lengthMenu" : [
                [5, 10, 20, -1],
                [5, 10, 20, 'All']
            ],
            "order" : [
                [1, "ASC"]
            ],
            "columnDefs":[{
                'orderable' : false,
                'targets' : [0, -1]
            }],
        });
        manageBranchTable.on('click', '.edit', function(){
            id = $(this).data('id');
            $("#branch_id").val(id);
            $.ajax({
                url : baseURL + filePath,
                method : "POST",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data : {
                    "branch_id" : id,
                    "fetch" : "branch"
                },
                dataType : "json",
                success : function(data){
                    $("#branch_id").val(id);
                    $("#name").val(data);
                }
            });
        });
        manageBranchTable.on('click', '.delete', function(){
            id = $(this).data('id');
            $("#record_id").val(id);
        });
    }
    return {
        init : function(){
            handleBranchTable();
        }
    }
}();

jQuery(document).ready(function(){
    TableDataTables.init();
})