var TableDataTables = function(){
    var handleSubjectTable = function(){
        var manageSubjectTable = $("#subject-datatable");
        var baseURL = window.location.origin;
        var filePath = "/ajax";
        var oTable = manageSubjectTable.dataTable({
            "processing" : true,
            "serverSide" : true,
            "paging": true,
            "scrollX": false,
            "ajax" : {
                url : baseURL + filePath,
                method : "POST",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data : {
                    "page": "manage_subject"
                }
            },
            "lengthMenu" : [
                [5, 10, 20, -1],
                [5, 10, 20, 'All']
            ],
            "order" : [
                [1, "ASC"]
            ],
            "columnDefs":[{
                'orderable' : false,
                'targets' : [0, -1]
            }],
        });
        manageSubjectTable.on('click', '.edit', function(){
            id = $(this).data('id');
            $("#subhect_id").val(id);
            $.ajax({
                url : baseURL + filePath,
                method : "POST",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data : {
                    "subject_id" : id,
                    "fetch" : "subject"
                },
                dataType : "json",
                success : function(data){
                    $("#subject_id").val(id);
                    $("#name").val(data);
                }
            });
        });
        manageSubjectTable.on('click', '.delete', function(){
            id = $(this).data('id');
            $("#record_id").val(id);
        });
    }
    return {
        init : function(){
            handleSubjectTable();
        }
    }
}();

jQuery(document).ready(function(){
    TableDataTables.init();
})