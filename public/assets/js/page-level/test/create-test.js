// console.log("here");

var baseURL = window.location.origin;
var filePath = "/ajax";

var branch_id = $('#branch').val();
var semester_id = $('#semester').val();
getSubjects();

$('#branch').change(function(){
    branch_id = this.value;
    $('#subject_id').value = -1;
    getSubjects();
});

$('#semester').change(function(){
    semester_id = this.value;
    $('#subject_id').value = -1;
    getSubjects();
});

function getSubjects()
{
    if(branch_id != 0 && semester_id != 0)
    {
        $('#subjects').empty();
        $.ajax({
            url: baseURL + filePath,
            method: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                "page": "create_test",
                "branchId": branch_id,
                "semesterId": semester_id
            },
            dataType: 'json',
            success: function(subjects){
                subject_id = $('#subject_id').val();

                $('#subjects').append(
                    `<option value="-1">SELECT</option>`
                );
                subjects.forEach(subject => {
                    if(subject_id == subject.id){
                        $('#subjects').append(`
                            <option value="${subject.id}" selected>${subject.name}</option>
                        `); 
                    }else{
                        $('#subjects').append(`
                            <option value="${subject.id}">${subject.name}</option>
                        `); 
                    }
                });
            }
        });
    }
}

$('#subjects').change(function(){
    var subject_id = this.value;
    $('#chapters').empty();
        $.ajax({
            url: baseURL + filePath,
            method: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                "page": "create_test",
                "subjectId": subject_id,
            },
            dataType: 'json',
            success: function(chapters){
                // console.log(chapters);
                // console.log(JSON.parse(chapters));
                fillOptionsWithClass("add-chapter-modal", chapters, "add-chapter");
                fillOptionsWithClass("edit-chapter-modal", chapters, "edit-chapter");
            }
        });
});

$("#2_marks_question_count").change(function(){
    updateTotalMarks();
});

$("#4_marks_question_count").change(function(){
    updateTotalMarks();
})

function updateTotalMarks(){
    // console.log($("#2_marks_question_count").val());
    total_marks = parseInt($("#2_marks_question_count").val()) * 2 + parseInt($("#4_marks_question_count").val()) * 4; 
    // console.log(total_marks);
    $("#total_marks").val(total_marks);
}

//jquery-validation

$(function(){
    $('#create-test-form').validate({
        'rules': {
            'name': {
                required: true,
                minlength: 2,
                maxlength: 255
            },
            'branch': {
                required: true,
            },
            'semester': {
                required: true,
            },
            'subjects': {
                required: true,
            },
            'chapter_id[]': {
                required: true,
            },
            '2_marks_question_count': {
                required: true,
                number: true
            },
            '4_marks_question_count': {
                required: true,
                number: true
            },
            'total_marks': {
                required: true,
            },
            'duration': {
                required: true,
            }
        },
        submitHandler: function(form){
            form.submit();
        }
    })
});

chapter_marks = 0;
$('#add-chapter').click(function(){
    blankAllChapterModalFields();
});

$('#add_chapter').click(function(){
    // console.log('Add Chapter');
    chapter_id = $('#add-chapter-modal').val();
    chapter_name = $(".chapter-"+chapter_id).html();
    // chapter = $('#chapter').val();
    marks = $("#add-chapter-marks").val();
    result = `
    <tr id="chapter_row_${chapter_id}">
        <td><input type="hidden" id="chapter_id_${chapter_id}" value="${chapter_id}" name="chapter_id[]">
            <input type="text" readonly class="form-control" name="chapter_name[]" id="chapter_name_${chapter_id}" value="${chapter_name}">
        </td>
        <td>
            <input type="text" name="chapter_marks[]" id="chapter_marks_${chapter_id}" class="form-control" readonly value="${marks}">
        </td>
        <td>
            <button type="button" class="create btn btn-sm btn-outline-warning edit-chapter" id="edit_chapter_id_${chapter_id}" data-id="${chapter_id}" data-toggle="modal" data-target="#editModal">
                <i class="nc-icon nc-ruler-pencil edit-chapter" data-id="${chapter_id}"></i>
            </button>  
            <button type="button" id="delete_chapter_id_${chapter_id}" data-id="${chapter_id}" class="create btn btn-sm btn-outline-danger delete-chapter" href="#">
                <i class="nc-icon nc-simple-remove delete-chapter" data-id="${chapter_id}"></i>
            </button>
        </td>
    </tr>`;
    $(".chapter-table").append(result);
    $('.chapter-'+chapter_id).addClass("d-none");
    $('.chapter-'+chapter_id).removeClass("chapter");
    $("#add-chapter-marks").val('');
    // console.log($('.chapter-'+chapter_id));
});

$("#manage-chapter-table").click(function(e){
    if(e.target.classList.contains("edit-chapter")){
        id = e.target.dataset.id;
        // console.log("here");
        // $("#chapter_selected").html();
        // console.log($("#chapter_name_"+id).html());
        // $("#marks_entered").html($("#chapter_marks_"+id).html());
        // console.log(id);
        $("#edit_chapter_id").val(id);
        $('.chapter-'+ id).addClass("chapter");
        $('.chapter-'+ id).removeClass("d-none");
        $("#edit-chapter-modal").val(id);
        // console.log($("#chapter_marks_"+id).val());
        $("#edit-modal-marks").val($("#chapter_marks_"+id).val());
    }
});

$('#editModal').on('hidden.bs.modal', function (e) {
    if ($("#edit_chapter_id").val() != "") {
        // console.log("Here");
        let chapterId = $("#edit_chapter_id").val();
        $(".chapter-" + chapterId).addClass("d-none").removeClass("chapter");
    }
});


$("#manage-chapter-table").click(function(e){
    if(e.target.classList.contains("delete-chapter")){
        id = e.target.dataset.id;
        $("#chapter_row_" + id).remove();
        $('.chapter-'+id).addClass("chapter");
        $('.chapter-'+id).removeClass("d-none");
    }
});

$("#edit_changes").click(function(){
    // console.log("hello");
    id = $("#edit-chapter-modal").val();
    // console.log(id);
    previous_id = $("#edit_chapter_id").val();
    // console.log("Chapter Name Selected" + ($("#chapter_name_"+previous_id).html()));
    
    $("#edit_chapter_id_"+previous_id).attr("data-id", id);
    $("#edit_chapter_id_"+previous_id).children().first().attr("data-id", id);
    $("#delete_chapter_id_"+previous_id).attr("data-id", id);
    $("#delete_chapter_id_"+previous_id).children().first().attr("data-id", id);

    $("#chapter_id_"+previous_id).attr("id", "chapter_id_"+id);
    $("#chapter_id_"+id).val(id);
    $("#chapter_row_"+previous_id).attr("id" , "chapter_row_"+id);
    $("#chapter_name_"+previous_id).attr("id" , "chapter_name_"+id);
    $("#chapter_marks_"+previous_id).attr("id" , "chapter_marks_"+id);
    $("#edit_chapter_id_"+previous_id).attr("id" , "edit_chapter_id_"+id);
    $("#delete_chapter_id_"+previous_id).attr("id" , "delete_chapter_id_"+id);

    $("#chapter_name_"+id).val($(".chapter-"+id).html());
    $("#chapter_marks_"+id).val($("#edit-modal-marks").val());
    $('.chapter-'+ id).addClass("d-none");
    $('.chapter-'+ id).removeClass("chapter");
    $('.chapter-'+ previous_id).addClass("chapter");
    $('.chapter-'+ previous_id).removeClass("d-none");

    $("#edit-chapter-modal").val($(".chapter.edit-chapter").first().val());
    $("#edit-modal-marks").val('');
    $("#edit_chapter_id").val('');
});

function fillOptionsWithClass(id, data, modal){
    res = '';
    res += "<option disabled selected>---SELECT---</option>";
    for(i=0; i<data.length; i++){
        // console.log(data[i].id);
        res += `<option class='chapter-${data[i].id} chapter ${modal}' value="${data[i].id}">${data[i].name}</option>`;
    }        
    $("#" + id).html(res);
    console.log($("#" + id).html());
}
function blankAllChapterModalFields(){
    // $("#add-chapter-modal").children().first().attr('selected', 'true');
    // console.log($(".chapter.add-chapter").first().val());
    $("#add-chapter-modal").val($(".chapter.add-chapter").first().val());
    $("#mark").val('');
}