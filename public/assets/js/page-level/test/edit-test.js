$(function(){

    var baseURL = window.location.origin;
    var filePath = "/ajax";

    var subject_id = $("#subject_id").val();
    var test_id = $("#test_id").val();

    var test_chapters;
    var test_chapters_name = [];

    $.ajax({
        url: baseURL + filePath,
        method: 'POST',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') 
        },
        data: {
            "page": "edit_test",
            "testId": test_id
        },
        dataType: 'json',
        success: function(data){
            test_chapters = data;
            test_chapters.forEach(test_chapter => {
                test_chapters_name.push(test_chapter.chapter_name);
            });
        }
    });

    $.ajax({
        url: baseURL + filePath,
        method: 'POST',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: {
            "page": "create_test",
            "subjectId": subject_id,
        },
        dataType: 'json',
        success: function(chapters){
            // console.log(test_chapters);
            var marks = 0;
            chapters.forEach(chapter => {
                if(test_chapters_name.includes(chapter.name))
                {
                    test_chapters.forEach(test_chapter => {
                        if(test_chapter.chapter_name == chapter.name)
                        {
                            marks = test_chapter.weightage_percentage;
                        }
                    });
                    result = `
                        <tr id="chapter_row_${chapter.id}">
                            <td><input type="hidden" id="chapter_id_${chapter.id}" value="${chapter.id}" name="chapter_id[]">
                                <input type="text" readonly class="form-control" name="chapter_name[]" id="chapter_name_${chapter.id}" value="${chapter.name}">
                            </td>
                            <td>
                                <input type="text" name="chapter_marks[]" id="chapter_marks_${chapter.id}" class="form-control" readonly value="${marks}">
                            </td>
                            <td>
                                <button type="button" class="create btn btn-sm btn-outline-warning edit-chapter" id="edit_chapter_id_${chapter.id}" data-id="${chapter.id}" data-toggle="modal" data-target="#editModal">
                                    <i class="nc-icon nc-ruler-pencil edit-chapter" data-id="${chapter.id}"></i>
                                </button>  
                                <button type="button" id="delete_chapter_id_${chapter.id}" data-id="${chapter.id}" class="create btn btn-sm btn-outline-danger delete-chapter" href="#">
                                    <i class="nc-icon nc-simple-remove delete-chapter" data-id="${chapter.id}"></i>
                                </button>
                            </td>
                        </tr>`;
                    $(".chapter-table").append(result);
                    fillOptionsWithClass("edit-chapter-modal", chapter, "edit-chapter")
                    $('.chapter-'+chapter.id).addClass("d-none");
                    $('.chapter-'+chapter.id).removeClass("chapter");                    
                }else{
                    fillOptionsWithClass("add-chapter-modal", chapter, "add-chapter");
                    fillOptionsWithClass("edit-chapter-modal", chapter, "edit-chapter");
                }
            });
            
        }
    });

    function fillOptionsWithClass(id, data, modal){
        res = '';
        res += `<option class='chapter-${data.id} chapter ${modal}' value="${data.id}">${data.name}</option>`;      
        $("#" + id).append(res);
    }

    

    $('#edit-test-form').validate({
        'rules': {
            'name': {
                required: true,
                minlength: 2,
                maxlength: 255
            },
            'branch': {
                required: true,
            },
            'semester': {
                required: true,
            },
            'subjects': {
                required: true,
            },
            'chapter_id[]': {
                required: true,
            },
            '2_marks_question_count': {
                required: true,
                number: true
            },
            '4_marks_question_count': {
                required: true,
                number: true
            },
            'total_marks': {
                required: true,
            },
            'duration': {
                required: true,
            }
        },
        submitHandler: function(form){
            form.submit();
        }
    });

});