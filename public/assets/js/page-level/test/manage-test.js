var TableDataTables = function(){
    var handleTestTable = function(){
        var manageTestTable = $("#test-datatable");
        var baseURL = window.location.origin;
        var filePath = "/ajax";
        var oTable = manageTestTable.dataTable({
            "processing" : true,
            "serverSide" : true,
            "paging": true,
            "scrollX": false,
            "ajax" : {
                url : baseURL + filePath,
                method : "POST",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data : {
                    "page": "manage_tests"
                }
            },
            "lengthMenu" : [
                [5, 10, 20, -1],
                [5, 10, 20, 'All']
            ],
            "order" : [
                [1, "ASC"]
            ],
            "columnDefs":[{
                'orderable' : false,
                'targets' : [0, -1]
            }],
        });

        manageTestTable.on('click', '.delete', function(){
            id = $(this).data('id');
            $("#record_id").val(id);
        });
    }
    return {
        init : function(){
            handleTestTable();
        }
    }
}();

jQuery(document).ready(function(){
    TableDataTables.init();
})