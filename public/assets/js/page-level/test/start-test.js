// console.log("here");

var baseURL = window.location.origin;
var filePath = "/ajax";

$(function() {
    var all_questions = undefined;

    var total_question_count = 0;
    var four_marks_question_count = 0;
    var two_marks_question_count = 0;
    var current_question = 0;

    var two_marks_easy_question_keys;
    var two_marks_medium_question_keys;
    var two_marks_hard_question_keys;
    var four_marks_easy_question_keys;
    var four_marks_medium_question_keys;
    var four_marks_hard_question_keys;


    let next_difficulty_level = "easy";

    let questions_displayed = [];

    var test_id = $('#test-id').val();

    //ajax call for all-questions - easy, medium, hard
    $.ajax({
        url: baseURL + filePath,
        method: 'POST',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: {
            "page": "start_test",
            "testId": test_id,
        },
        dataType: 'json',
        success: function(questions){
            all_questions = questions;

            two_marks_question_count = all_questions['2-marks']['question-count'];
            four_marks_question_count = all_questions['4-marks']['question-count'];
            total_question_count = all_questions['2-marks']['question-count'] + all_questions['4-marks']['question-count'];

            two_marks_easy_question_keys = Object.keys(all_questions['2-marks']['easy']['question']);
            two_marks_medium_question_keys = Object.keys(all_questions['2-marks']['medium']['question']);
            two_marks_hard_question_keys = Object.keys(all_questions['2-marks']['hard']['question']);

            four_marks_easy_question_keys = Object.keys(all_questions['4-marks']['easy']['question']);
            four_marks_medium_question_keys = Object.keys(all_questions['4-marks']['medium']['question']);
            four_marks_hard_question_keys = Object.keys(all_questions['4-marks']['hard']['question']);

            selectNextQuestion();
        }
    });

    if(document.getElementById("duration")){
        var countDownDate = new Date(new Date().getTime() + (parseInt(document.getElementById("duration").value) * 3600000));


        // Update the count down every 1 second
        var x = setInterval(function() {

            // Get today's date and time
            var now = new Date().getTime();

            // Find the distance between now and the count down date
            var distance = countDownDate - now;

            // Time calculations for hours, minutes and seconds
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);

            // Display the result in the element with id="demo"
            if(document.getElementById("timer")){
                document.getElementById("timer").innerHTML = "Time Remaining : " + hours + ":"+ minutes + ":" + seconds;

                // If the count down is finished, write some text
                if (distance < 0) {
                    clearInterval(x);
                    document.getElementById("timer").innerHTML = "TIME UP!";
                }
            }
        }, 1000);
    }

    $('#save-continue-btn').on('click', function(){
        current_question = current_question + 1;
        console.log(questions_displayed);

        if(current_question == total_question_count-1)
            manageSaveAndContinueButton();
        manageQuestions(questions_displayed[current_question-1]);
        selectNextQuestion();

    });

    //managing the disabling property of the save and continue button
    function manageSaveAndContinueButton()
    {
        $('#save-continue-btn').addClass('disabled');
        $('#submit-btn').removeClass('disabled');
    }

    /*
    * main logic
    */
    function selectNextQuestion()
    {
        var result_array = [];
        var marks_array = ['2', '4'];
        
        var marks = marks_array[Math.floor(Math.random() * marks_array.length)];
        if(marks == '2')
        {
            if(two_marks_question_count == 0)
            {
                result_array = all_questions['4-marks'][next_difficulty_level];
                four_marks_question_count--;
            }else{
                result_array = all_questions['2-marks'][next_difficulty_level];
                two_marks_question_count--;
            }
        }else if(marks == '4'){
            if(four_marks_question_count == 0)
            {
                result_array = all_questions['2-marks'][next_difficulty_level];
                two_marks_question_count--;
            }else{
                result_array = all_questions['4-marks'][next_difficulty_level];
                four_marks_question_count--;
            }
        }

        setTest(result_array, next_difficulty_level);
    }

    function setTest(questions_array, difficult_level)
    {
        var keys_array = Object.keys(questions_array['question']);

        var question_id =  keys_array[Math.floor(Math.random() * keys_array.length)];

        var index = keys_array.indexOf(question_id);

        keys_array.splice(index, 1);

        addQuestion(question_id, 
                    questions_array['question'][question_id], 
                    questions_array['options'][question_id], 
                    questions_array['correct-option'][question_id], 
                    difficult_level);
    }

   /*
   * question_array is the current question and its info :
            question_array = {
                "correct_option": ,
                "correct_option_id": ,
                "selected_option": ,
                "selected_option_id":,
                "difficulty_level":  
            };

    * This function checks the array and determines what should be the difficulty level of the next question
   */
    function manageQuestions(question_array)
    {
       var levels = ['easy', 'medium', 'hard'];
        if(question_array['correct_option_id'] === question_array['selected_option_id'])
        {
            for(i=0; i<levels.length; i++)
            {
                if(question_array['difficult_level'] == levels[i])
                {
                    if(levels[i] == levels.length)
                        next_difficulty_level = levels[i];
                    else
                        next_difficulty_level = levels[i+1];
                }
            }

        }else{
            if(question_array['difficult_level'] == levels[length-1])
                next_difficulty_level = level[0];
            else    
                next_difficulty_level = question_array.difficult_level;
        }
    }

    function addQuestion(question_id, question, options, correct_option_id, difficulty_level)
    {
        if(question != undefined && options != undefined)
        {
            var options_numering = ['a', 'b', 'c', 'd'];
            var correct_option = "";
            var data = "";
            data += `
                <div id="question">
                    <div class="row">
                        <div id="statement" class="mr-3 ml-3">
                            <h5 id="question-${current_question}">${question}</h5>
                            <input type="hidden" name="question_id" id="question-${current_question}-id" value="${question_id}">
                        </div>
                    </div>
                    <div class="row">
            `;

            i = 0;
            for(index in options)
            {
                if(index == correct_option_id)
                {
                    correct_option = options_numering[i];
                }
                data += `
                            <div class="col-md-6">
                                <div class="options">
                                    <div id="statement">
                                        <div class="form-check">
                                            <label class="form-check-label">
                                                <input class="form-check-input selected_option" 
                                                       type="radio" 
                                                       name="question-${current_question}" 
                                                       id="option-${i+1}" 
                                                       value="${options_numering[i]}" 
                                                       data-id="${index}" 
                                                       data-question="${current_question}"
                                                >
                                                    ${options_numering[i]}. ${options[index]}
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                `;
                i++;
            }

            data += `
                        </div>
                    </div>
            `;

            $('#all-questions').empty();
            $('#all-questions').append(data);

            questions_displayed[current_question] = {
                "question_id": question_id,
                "correct_option": correct_option,
                "correct_option_id": correct_option_id,
                "selected_option": "",
                "selected_option_id": "",
                "difficult_level": difficulty_level
            };
        }
    }

    $("#all-questions").on('change', '.selected_option',  function(){
        var selected_option = this.value;
        var selected_option_id = Number($(this).attr('data-id'));

        questions_displayed[current_question].selected_option = selected_option;
        questions_displayed[current_question].selected_option_id = selected_option_id;
        
    });


    $('#submit-btn').on('click', function(){
        //ajax call for sending the details of all the questions
        
        $.ajax({
            url: "/test/result",
            method: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                "page": "test_result",
                "testId": test_id,
                "testData": questions_displayed
            },
            dataType: 'json',
            success: function(response){
                //notify that test submitted successfully

                if(response) //== "success")
                {
                    // If test submitted successfully it will come here
                    console.log("Hello");
                    console.log(response);
                }
            }
        });
    });
});