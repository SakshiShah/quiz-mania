
<footer class="footer footer-black footer-own">
    <div class="container-fluid">
        <div class="row">
            <div class="credits m-auto">
                <span class="copyright">
                © <script>
                    document.write(new Date().getFullYear())
                </script> Quiz Mania
                </span>
            </div>
        </div>
    </div>
</footer>