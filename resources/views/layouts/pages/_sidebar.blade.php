<div class="sidebar" data-color="white" data-active-color="danger">
    <div class="logo">
    <a href="https://www.creative-tim.com" class="simple-text logo-mini">
        <div class="logo-image-small">
        <img src="../assets/img/logo-small.png">
        </div>
        <!-- <p>CT</p> -->
    </a>
    <a href="#" class="simple-text logo-normal">
        Quiz Mania
        <!-- <div class="logo-image-big">
            <img src="../assets/img/logo-big.png">
        </div> -->
    </a>
    </div>
    <div class="sidebar-wrapper pb-5">
    <ul class="nav">
        <li class="active ">
            <a href="{{ route('home') }}">
                <i class="nc-icon nc-bank"></i>
                <p>Dashboard</p>
            </a>
        </li>

        <li class="">
            <a href="{{ route('tests.index') }}">
                <i class="nc-icon nc-book-bookmark"></i>
                <p>Test</p>
            </a>
        </li>

        @if(@auth()->user()->isAdmin())

            <li class=" ">
                <a href="#">
                    <i class="nc-icon nc-circle-10"></i>
                    <p>Students</p>
                </a>
            </li>

            <li class=" ">
                <a href="{{ route('branches.index') }}">
                    <i class="nc-icon nc-hat-3"></i>
                    <p>Branches</p>
                </a>
            </li>

            <li class=" ">
            <a href="{{ route('subjects.index') }}">
                    <i class="nc-icon nc-paper"></i>
                    <p>Subjects</p>
                </a>
            </li>

            
            <li class=" ">
                <a href="#">
                    <i class="nc-icon nc-align-left-2"></i>
                    <p>Questions</p>
                </a>
            </li>
        @endif
        </ul>
    </div>
</div>