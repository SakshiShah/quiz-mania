@extends('layouts.app')

@section('content')
    <div class="row mt-5">
        <div class="col-md-12">
            <div class="pull-right clearfix">
                <a href="{{ route('subjects.index') }}" class="btn btn-secondary m-1"><i
                        class="nc-icon nc-paper mr-1"></i> Manage Subject
                </a>
            </div>
        </div>
    </div>

    <div class="row mt-3">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title">Create Subject</h4>
                </div>
                <div class="card-body">
                    <form action="{{ route('subjects.store') }}" method="POST">

                        @csrf
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label for="name" class="col-form-label text-md-right text-primary">{{ __('Subject Name') }}</label>

                                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                        @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                </div>
                            </div>
                        </div>

                        <div class="pull-right clearfix">
                            <button type="submit" class="btn btn-primary m-1"><i
                                    class="nc-icon nc-check-2 mr-1"></i> Create
                            </button>
                        </div> 
    
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection