@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 mb-1">
            <div class="pull-right clearfix">
                <a href="{{ route('subjects.create') }}" class="btn btn-secondary m-1"><i
                        class="nc-icon nc-simple-add mr-1"></i> Create Subject
                </a>
            </div>
        </div>
        
        <div class="col-md-12 mt-3">
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title">Subjects</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table" id="subject-datatable">
                            <thead class="text-primary">
                            <tr>
                                <th>
                                    #
                                </th>
                                <th>
                                    Name
                                </th>
                                <th>
                                    Actions
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--EDIT MODAL-->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Subject</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="#" method="POST" id="editForm">
                @csrf
                @method('PUT')
                <div class="modal-body">
                    <div class="form-group-row">
                        <input type="hidden" name="branch_id" id="branch_id">
                        <label for="name" class="col-sm-12">Subject Name</label>
                        <input type="text" class="form-control" id="name" name="name">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light m-1" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger m-1">Edit</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!--END EDITS MODAL-->

<!--DELETE MODAL-->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Delete Subject</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="" method="POST" id="deleteForm">
                @csrf
                @method('DELETE')
                <div class="modal-body">
                    <input type="hidden" name="record_id" id="record_id">
                    <p>Are you sure want to delete Subject??</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light m-1" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger m-1">Delete</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!--END DELETE MODAL-->
@endsection



@section('styles')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
    <style>
        table{
            width: 100% !important;
        }
    </style>
@endsection

@section('scripts')
    <script src="{{ asset('assets/vendor/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/js/page-level/subject/manage-subject.js') }}"></script>
    <script>
        function displayEditModalForm(branchID) {
            var url = '/subjects/' + branchID;
            $("#editForm").attr('action', url);
        }
        function displayDeleteModalForm(branchID) {
            var url = '/trash/' + branchID
            $("#deleteForm").attr('action', url);
        }
    </script>
@endsection
