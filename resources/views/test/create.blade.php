@extends('layouts.app')

@section('content')
    <div class="row mt-5">
        <div class="col-md-12">
            <div class="pull-right clearfix">
                <a href="{{ route('tests.index') }}" class="btn btn-secondary m-1"><i
                        class="nc-icon nc-paper mr-1"></i> Manage Test
                </a>
            </div>
        </div>
    </div>

    <div class="row mt-3">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title">Create Test</h4>
                </div>
                <div class="card-body">
                    <form action="{{ route('tests.store') }}" method="POST" id="create-test-form">

                        @csrf

                        <div id="create-test">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label for="name" class="col-form-label text-md-right text-primary">{{ __('Test Name') }}</label>

                                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                            @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="branch" class="col-form-label text-md-right text-primary">{{ __('Branch') }}</label>

                                            <select id="branch" type="text" class="form-control @error('branch') is-invalid @enderror" name="branch" autofocus>
                                                <option value="-1" disabled selected>SELECT</option>
                                                @foreach($branches as $branch)
                                                    <option value="{{$branch->id}}">{{$branch->name}}</option>
                                                @endforeach
                                            </select>

                                            @error('branch')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                    </div>
                                </div>
                    
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="semester" class="col-form-label text-md-right text-primary">{{ __('Semester') }}</label>

                                            <select id="semester" type="text" class="form-control @error('semester') is-invalid @enderror" name="semester" autofocus>
                                                <option value="-1" disabled selected>SELECT</option>
                                                @foreach($semesters as $semester)
                                                    <option value="{{$semester->id}}">{{$semester->name}}</option>
                                                @endforeach
                                            </select>

                                            @error('semester')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="subjects" class="col-form-label text-md-right text-primary">{{ __('Subjects') }}</label>

                                            <select id="subjects" type="text" class="form-control @error('subjects') is-invalid @enderror" name="subjects" autofocus>
                                                <option value="-1">SELECT</option>
                                            </select>

                                            @error('subjects')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                    </div>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <label for="chapter" class="col-form-label text-md-right text-primary">Chapter </label>
                                    <button type="button" class="btn btn-primary" id="add-chapter" data-toggle="modal" data-target="#addModal"><i class="fa fa-plus"></i> Add Chapter</button>
                                    <table class="table table-bordered" id="manage-chapter-table" width="100%" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th>Chapter Title</th>
                                                <th>Chapter Weightage</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody class="chapter-table"></tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="Number Of Question for 2 Marks" class="col-form-label text-md-right text-primary">Number Of Question for 2 Marks</label>
                                        <input id="2_marks_question_count" type="number" class="form-control @error('2_marks_question_count') is-invalid @enderror" name="two_marks_question_count" value="{{ old('2_marks_question_count') ? old('2_marks_question_count') : 0 }}" required autocomplete="2_marks_question_count" autofocuse>
                                        @error('2_marks_question_count')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="Number Of Question for 4 Marks" class="col-form-label text-md-right text-primary">Number Of Question for 4 Marks</label>
                                        <input id="4_marks_question_count" type="number" class="form-control @error('2_marks_question_count') is-invalid @enderror" name="four_marks_question_count" value="{{ old('4_marks_question_count') ? old('4_marks_question_count') : 0 }}" required autocomplete="4_marks_question_count" autofocuse>
                                        @error('4_marks_question_count')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="total_marks" class="col-form-label text-md-right text-primary">{{ __('Total Marks') }}</label>

                                            <input id="total_marks" type="number" class="form-control @error('total_marks') is-invalid @enderror" name="total_marks" value="{{ old('total_marks') ? old('total_marks') : 0}}" required autocomplete="total_marks" autofocus readonly>

                                            @error('total_marks')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="duration" class="col-form-label text-md-right text-primary">Duration <span class="text-muted">[Hours]</span></label>

                                            <input id="duration" type="number" class="form-control @error('duration') is-invalid @enderror" name="duration" value="{{ old('duration') }}" required autocomplete="duration" autofocus>

                                            @error('duration')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label for="scheduled_at" class="col-form-label text-md-right text-primary">Scheduled At <span class="text-muted">[Optional]</span></label>

                                            <input id="scheduled_at" type="datetime-local" class="form-control @error('scheduled_at') is-invalid @enderror" name="scheduled_at" value="{{ old('scheduled_at') }}" autocomplete="duration" autofocus>

                                            @error('scheduled_at')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="pull-right clearfix">
                                <button type="submit" class="btn btn-primary m-1"><i
                                        class="nc-icon nc-check-2 mr-1"></i> Create
                                </button>
                            </div> 

                        </div>
    
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addtModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="addtModalLabel">Add Chapter</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" name="chapter_id" id="chapter_id">
                        <div class="form-group-row">
                            <div class="form-group col-md-10">
                                <label for="chapters">Chapters</label>
                                <select name="chapter" id="add-chapter-modal" class="form-control chapter-list">
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="Marks">Enter Weightage in %</label>
                                <input type="number" id="add-chapter-marks" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" name="add_chapter" data-dismiss="modal" id="add_chapter">Add Chapter</button>
                    </div>
                </div>
            </div>
        </div>
        <!--/ADD CHAPTER MODAL-->


        <!--EDIT CHAPTER MODAL-->
        <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="editModalLabel">Edit</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" name="chapter_id" id="edit_chapter_id">
                        <div class="form-group-row">
                            <div class="form-group col-md-10">
                                <label for="Chapter">Edit Chapter</label>
                                <select name="chapter" id="edit-chapter-modal" class="form-control chapter-list">
                                </select>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="Marks">Edit Weightage in %</label>
                            <input type="number" id="edit-modal-marks" class="form-control">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-danger" id="edit_changes" data-dismiss="modal" name="save_changes">Save Changes</button>
                    </div>
                </div>
            </div>
        </div>
        <!--/EDIT CHAPTER MODAL-->
@endsection

@section('scripts')
    <script src="{{ asset('assets/js/page-level/test/create-test.js') }}"></script>
@endsection