@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row mt-5">

        @if(auth()->user()->isAdmin())
            <div class="col-md-12 mb-1">
                <div class="pull-right clearfix">
                    <a href="{{ route('tests.create') }}" class="btn btn-secondary m-1"><i
                            class="nc-icon nc-simple-add mr-1"></i> Create Test
                    </a>
                </div>
            </div>
        @endif
        
        @if(auth()->user()->isAdmin())
            <div class="col-md-12 mt-3">
                <div class="card">
                    <div class="card-header card-header-primary">
                        <h4 class="card-title">Tests</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table" id="test-datatable">
                                <thead class="text-primary">
                                <tr>
                                    <th>
                                        #
                                    </th>
                                    <th>
                                        Name
                                    </th>
                                    <th>
                                        Actions
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        @else
            @foreach($tests as $test)
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header card-header-primary m-2">
                            <h5>{{$test->name}}</h5>
                        </div>
                        <div class="card-body">
                            <p>Total Marks: {{$test->total_marks}}</p>
                            <p>Duration: {{ $test->duration }}</p>
                            @if($test->scheduled_at == NULL || (Carbon\Carbon::parse($test->scheduled_at)->diffInMinutes(Carbon\Carbon::now()) <= 5 && Carbon\Carbon::parse($test->scheduled_at)->diffInMinutes(Carbon\Carbon::now()) >= -10))
                            
                            <form action="{{ route('tests.startTest') }}" method="POST">
                            @csrf 
                                <input type="hidden" name="test-id" id="test-id" value="{{ $test->id }}">
                                <div>
                                    <button type="submit" class="btn btn-primary m-1"><i
                                            class="nc-icon nc-check-2 mr-1"></i> Start Test
                                    </button>
                                </div>
                            </form> 
                            @else
                                <p>Scheduled At: <span id="scheduled_at">{{$test->scheduled_at}}</span></p>
                                
                            @endif
                                
                        </div>
                    </div>
                </div>
            @endforeach
        @endif
    </div>
</div>
    <!--DELETE MODAL-->
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Delete Test</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="" method="POST" id="deleteForm">
                    @csrf
                    @method('DELETE')
                    <div class="modal-body">
                        <p>Are you sure want to delete Test??</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light m-1" data-dismiss="modal"><i class="material-icons">clear</i> Close</button>
                        <button type="submit" class="btn btn-danger m-1"><i class="material-icons">delete</i> Delete</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--END DELETE MODAL-->
@endsection



@section('styles')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
    <style>
        table{
            width: 100% !important;
        }
    </style>
@endsection

@section('scripts')
    <script src="{{ asset('assets/vendor/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/js/page-level/test/manage-test.js') }}"></script>
    <script>
        function displayModalForm(testID) {
            var url = '/test/' + testID + '/trash';
            $("#deleteForm").attr('action', url);
        }
    </script>

    <script>
        var d1 = new Date($("#scheduled_at").html());
        var dummy = new Date();
        dummy.setTime(d1.getTime());
        dummy.setMinutes(dummy.getMinutes() + 10);
        var myVar = setInterval(giveTest, 1000)
        function giveTest(){
            $("#give-test-btn").empty();
            var d2 = Date.now();
            if((d1.getTime() - d2) > 900000 || (((dummy.getTime() - d2) < 600000 ) && ((dummy.getTime() - d2) > 0 ))){
                clearInterval(myVar);
            }
        }

        var myVar2 = setInterval(clearBTN, 1000);

        function clearBTN(){
            if(dummy.getTime() <= Date.now()){
                clearInterval(myVar2);
                clearInterval(myVar);
                $("#give-test-btn").empty();
            }
        }
    </script>
@endsection