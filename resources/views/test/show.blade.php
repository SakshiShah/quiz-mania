@extends('layouts.app')

@section('content')
    <div class="row mt-5">
        <div class="col-md-12">
            <div class="pull-right clearfix">
                <a href="{{ route('tests.index') }}" class="btn btn-secondary m-1"><i
                        class="nc-icon nc-paper mr-1"></i> Manage Test
                </a>
            </div>
        </div>
    </div>

    <div class="row mt-3">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title">Create Test</h4>
                </div>
                <div class="card-body">

                        <div id="create-test">
                            <input type="hidden" name="test_id" id="test_id" value="{{$test->id }}">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label for="name" class="col-form-label text-md-right text-primary">{{ __('Test Name') }}</label>

                                        <input id="name" 
                                            type="text" 
                                            class="form-control-plaintext" 
                                            name="name" 
                                            value="{{ $test->name }}" 
                                            required
                                            disabled 
                                            autocomplete="name"     
                                        >
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="branch" class="col-form-label text-md-right text-primary">{{ __('Branch') }}</label>

                                        <input id="branch" 
                                            type="text" 
                                            class="form-control-plaintext" 
                                            name="branch" 
                                            value="{{ $branch->name }}" 
                                            required 
                                            autocomplete="branch"  
                                            disabled
                                        >

                                    </div>
                                </div>
                    
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="semester" class="col-form-label text-md-right text-primary">{{ __('Semester') }}</label>

                                        <input id="semester" 
                                            type="text" 
                                            class="form-control-plaintext" 
                                            name="semester" 
                                            value="{{ $semester->name }}" 
                                            required 
                                            autocomplete="semester"  
                                            disabledh
                                        >

                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="subjects" class="col-form-label text-md-right text-primary">{{ __('Subject') }}</label>

                                        <input id="subjects" 
                                            type="text" 
                                            class="form-control-plaintext" 
                                            name="subjects" 
                                            value="{{ $subject->name }}" 
                                            required 
                                            autocomplete="subjects"  
                                            disabled
                                        >
                                    </div>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <label for="chapter" class="col-form-label text-md-right text-primary">Chapter & its weightage(%)</label>

                                    @foreach($chapter_weightages as $chapter_weightage)
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input type="text" disabled class="form-control-plaintext" name="chapter_name[]" id="chapter_name" value="{{ $chapter_weightage['chapter_name'] }}">
                                            </div>
                                            <div class="col-md-6">
                                                <input type="text" name="chapter_marks[]" id="chapter_marks}" class="form-control-plaintext" disabled value="{{ $chapter_weightage['wieghtage'] }}">
                                            </div>
                                        </div>                                    
                                    @endforeach
                                    
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="Number Of Question for 2 Marks" class="col-form-label text-md-right text-primary">Number Of Question for 2 Marks</label>

                                        <input id="2_marks_question_count" 
                                            type="number" 
                                            class="form-control-plaintext"
                                            name="two_marks_question_count"
                                            value="{{ $test->two_marks_question_count }}"
                                            required 
                                            autocomplete="2_marks_question_count" 
                                            autofocuse
                                            disabled
                                        >
                                        
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="Number Of Question for 4 Marks" class="col-form-label text-md-right text-primary">Number Of Question for 4 Marks</label>

                                        <input id="4_marks_question_count" 
                                            type="number" 
                                            class="form-control-plaintext" 
                                            name="four_marks_question_count" 
                                            value="{{ $test->four_marks_question_count }}" 
                                            required 
                                            autocomplete="4_marks_question_count" 
                                            autofocuse
                                            disabled
                                        >
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="total_marks" class="col-form-label text-md-right text-primary">{{ __('Total Marks') }}</label>

                                        <input id="total_marks" 
                                            type="number" 
                                            class="form-control-plaintext" 
                                            name="total_marks" 
                                            value="{{ $test->total_marks }}" 
                                            required 
                                            autocomplete="total_marks"  
                                            disabled
                                        >
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="duration" class="col-form-label text-md-right text-primary">Duration <span class="text-muted">[Hours]</span></label>

                                        <input id="duration" 
                                            type="number" 
                                            class="form-control-plaintext" 
                                            name="duration" 
                                            value="{{ $test->duration }}" 
                                            required 
                                            autocomplete="duration" 
                                            disabled
                                        >
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label for="scheduled_at" class="col-form-label text-md-right text-primary">Scheduled At <span class="text-muted">[Optional]</span></label>

                                        <input id="scheduled_at" 
                                            type="datetime-local" 
                                            class="form-control-plaintext" 
                                            name="scheduled_at" 
                                            value="{{ $test->scheduled_at }}" 
                                            autocomplete="duration"  
                                            disabled
                                        >
                                    </div>
                                </div>
                            </div>

                        </div>
                </div>
            </div>
        </div>
    </div>

@endsection
