@extends('layouts.test-app')

@section('content')
     
    <div class="container mt-5">
        <input type="hidden" name="test-id" id="test-id" value="{{ $test->id }}">
        <input type="hidden" name="scheduled_at" id="scheduled_at" value="{{ $test->scheduled_at }}">
        <input type="hidden" name="duration" id="duration" value="{{ $test->duration }}">
        <div class="test-title">
            <div class="d-flex justify-content-between">
                <h6>{{ $test->name }}</h6>
                @if($test->scheduled_at)
                    <p id="timer"></p>
                @endif
            </div>
        </div>
        <div class="test-details">
            <div class="d-flex justify-content-between">
                <div>
                    <p class="text-muted">Subject: {{ $subject_name }}</p>
                </div>
                <div>
                    <p class="text-muted">Total Marks: {{ $test->total_marks }}</p>
                </div>
            </div>
        </div>

        <hr>  
        <div class="row">
            <div id="all-questions">
                
            </div>
        </div>

        <div class="justify-content-between text-center mt-3">
            <a id="save-continue-btn" class="btn btn-secondary text-white">Save and Continue</a>
        </div>

        <div class="row mt-3">
            <div class="col-md-12">
                <div class="pull-right clearfix">
                    <input type="submit" id="submit-btn" class="btn btn-primary disabled" value="Submit">
                </div>
            </div>
        </div>
    </div>  
@endsection

@section('scripts')
    <script src="{{ asset('assets/js/page-level/test/start-test.js') }}"></script>
@endsection