<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware(['auth'])->group(function(){
    Route::get('/dashboard', function () {
        return view('layouts.app');
    });
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('tests', 'TestsController');
Route::post('/tests/start', 'TestsController@startTest')->name('tests.startTest');

Route::resource('branches', 'BranchesController');
Route::delete('/trash/{branch}', 'BranchesController@trash')->name('branches.trash');

Route::resource('subjects', 'SubjectsController');
// Route::delete('/trash/{subject}', 'SubjectsController@trash')->name('subjects.trash');

Route::post('/test/result', 'AjaxController@getResult')->name('test.result');

Route::post('/ajax', 'AjaxController@getData');